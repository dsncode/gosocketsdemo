# Go sockets Demo

this demo, uses Gin and Gorilla to show websockets in go

## How to run it

if you are using go 1.10, first install these dependencies.

First install gin and gorilla
```bash
go get github.com/gin-gonic/gin
```

Install gorilla
```bash
go get github.com/gorilla/websocket
```


Then just run it

```bash
go run main.go
```

Then visit http://127.0.0.1:8080  and see the example working