module gitlab.com/dsncode/gosocketsdemo

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/gorilla/websocket v1.4.0
)
